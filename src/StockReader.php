<?php
/**
 * File: StockReader.php
 *
 * @author mrx <mma@shopall24.com>
 * 
 * @package DataAccessStockReader
 * @subpackage StockReader
 * @version 1.0.3
 */

namespace DataAccessStockReader;

// User Data Access Layer
use DataAccessDAL\DAL;
use Core\Tool\XML2Array;

/**
 * Class StockReader
 * Stock Reader is the specialized class that serve  the data
 * in the inventory/stock page. 
 * It uses an XML file to access stock data and the DAL class to access
 * GS1/SA24 data. 
 *
 * @package DataAccessStockReader
 * @subpackage StockReader
 * @version 1.0.3
 */
class StockReader
{
    private static $productXPath = "//StockLevel";

    public static function stockRead($gln) 
    {
        // Data-source XML-stock-data "navision"
        libxml_use_internal_errors(true);
        $xml = simplexml_load_file(getenv('XMLDATASOURCE_NAVISION'));
        if ($xml === false) {
            foreach(libxml_get_errors() as $error) {
                $message = $error->message . "cwd: " . getcwd();
                break;
            }
            throw new \Exception($message, 500);
        }

        $stockLevelXPath = "//StockLevel[ GLN='$gln' ]";
        $aStockLevelSXEs = $xml->xpath($stockLevelXPath);

        $aXMLs = array();
        foreach($aStockLevelSXEs as $stockLevelSXE) {
            $aXMLs[] = XML2Array::createArray($stockLevelSXE->asXML());
        }

        //file_put_contents('asXML.txt' , var_export( $aXMLs, true ));
        
        // DAL
        $dal = DAL::getInstance();


        // Collectors
        $rowInit = [
            "stockLevel" => '1',
            'sku'=> '',
            'gln'=> $gln,
            'name' => 'Product name not loaded in report',
            'location' => '',
            'available_quantity' => '',
            'remaining_qty' => '',
            'expiry_date' => '',
            'notification_days' => '',
        ];
        $allRows = array();

        // Happy cycle
        foreach ($aXMLs as $i => $aXML) {
            
            // Collector
            $rows = array();

            

            //var_dump($aXML);

            // Only expiring-dates
            if(!isset($aXML['StockLevel']['ExpiryDates'])) {
                //continue;
            }

            // GS1/SA24_catalogue
            $filters = ['filters' => ['tradeItem.gtin' => $aXML['StockLevel']['APSKUNo']]];
            $result = $dal->catalogueItemRead($filters);

            // ### Main Row
            $row = $rowInit;
            // GTIN
            $row['sku'] = $aXML['StockLevel']['APSKUNo'];
            // Name (short description)
            if(!empty($result['GS1']) && sizeof($result['GS1']) > 0) {
                $document = (array) array_pop($result['GS1']);
                $document = json_decode(json_encode($document), true);
                $row['name'] = (string) (isset(
                    $document['tradeItem']['tradeItemInformation']['extension']
                            ['trade_item_description:tradeItemDescriptionModule']
                            ['tradeItemDescriptionInformation']['descriptionShort']['@value'])) 
                        ? $document['tradeItem']['tradeItemInformation']['extension']
                            ['trade_item_description:tradeItemDescriptionModule']
                            ['tradeItemDescriptionInformation']['descriptionShort']['@value']
                        : $rowInit['name'] ; 
            }
            if(!empty($result['SA24']) && sizeof($result['SA24']) > 0) {
                $document = (array) array_pop($result['SA24']);
                $document = json_decode(json_encode($document), true);
                $row['name'] = (string) (isset(
                    $document['tradeItem']['tradeItemInformation']['extension']
                            ['trade_item_description:tradeItemDescriptionModule']
                            ['tradeItemDescriptionInformation']['descriptionShort']['@value'])) 
                        ? $document['tradeItem']['tradeItemInformation']['extension']
                            ['trade_item_description:tradeItemDescriptionModule']
                            ['tradeItemDescriptionInformation']['descriptionShort']['@value']
                        : $row['name'] ; 
            }
            /*
            file_put_contents('GS1.txt', var_export($document['tradeItem']['tradeItemInformation']['extension']
                            ['trade_item_description:tradeItemDescriptionModule']
                            ['tradeItemDescriptionInformation'] , true));
            */
            // StockLevel->Location
            $row['location'] = $aXML['StockLevel']['Location'];
            // AvailableQuantity
            $row['available_quantity'] = $aXML['StockLevel']['AvailableQuantity'];
            // Collect the Main row/StockLevel
            $stockLevelrow = $row;

            // expiry_date
            $stockLevelrow['expiry_date'] = '';
            
            // ### Nested Rows
            $nesteds = array();
            $aXMLNesteds = null;
            // Form moonlight lovers only
            if(isset($aXML['StockLevel']['ExpiryDates'])) {


                // stockLevel (row type)
                $row['stockLevel'] = '0';
                if(isset($aXML['StockLevel']['ExpiryDates']['Lot'])) {
                    if( array_key_exists('ExpiryDate', $aXML['StockLevel']['ExpiryDates']['Lot'])) { 
                        $aXMLNesteds[] = $aXML['StockLevel']['ExpiryDates']['Lot'];
                    } else {
                        $aXMLNesteds = $aXML['StockLevel']['ExpiryDates']['Lot'];
                        //var_dump($aXMLNesteds);
                        //echo "THERE";
                    }
                }

                // file_put_contents("nested.txt", var_export( $aXMLNesteds, true ));


                // nested row reset
                //$nesteds = array();
                foreach($aXMLNesteds as $aXMLNested) {

                    // Row init
                    $row = $rowInit;

                    // stockLevel
                    $row['stockLevel'] = '0' ;

                    // GTIN/EAN (sku)
                    $row['sku'] = $stockLevelrow['sku'];

                    // description (short name)
                    $row['name'] = $stockLevelrow['name'];

                    // ExpiryDates->Lot->Shelf
                    $row['location'] = (isset($aXMLNested['Shelf']))
                            ? $aXMLNested['Shelf']
                            : '';

                    // RemainingQty
                    if(array_key_exists('RemainingQty', $aXMLNested)) {
                        $row['remaining_qty'] = $aXMLNested['RemainingQty'];
                    }

                    // ExpiryDate
                    if(array_key_exists('ExpiryDate', $aXMLNested)) {
                        $row['expiry_date'] = $aXMLNested['ExpiryDate'];
                    }
                    
                    // NotificationDays
                    $row['notification_days'] = (string) (isset(
                        $document['tradeItem']['tradeItemInformation']['extension']
                                ['trade_item_stock_information:tradeItemStockInformationModule']
                                ['warehouse']['rules']['notificationDays'])) 
                            ? $document['tradeItem']['tradeItemInformation']['extension']
                                ['trade_item_stock_information:tradeItemStockInformationModule']
                                ['warehouse']['rules']['notificationDays']
                            : '0' ; 

                    // Collect Nested Rows
                    $nesteds[] = $row;
                }

                // copy ExpiryDates->Lot informations in the StockLevel
                $lowerDate = null;
                $remainingQty = 0;

                $DlowerDate = \DateTime::createFromFormat('Y-m-d', '3000-01-01');
                try{
                    foreach ($nesteds as $i => $nested) {
  
                        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $nested['expiry_date'])) {

                            $notificationDays = $nested['notification_days'];
                            // may be less
                            if(!empty($notificationDays)) {
                                $DmaybeLessDate = \DateTime::createFromFormat('Y-m-d', $nested['expiry_date']);
                                $interval = "P" . trim($notificationDays) . "D";
                                // file_put_contents( "interval$i.txt",  $interval );
                                $Dinterval  =  new \DateInterval($interval);
                                $DmaybeLessDate->sub($Dinterval); 
                                if( $DmaybeLessDate < $DlowerDate ) {
                                    $lowerDate =  $DmaybeLessDate;
                                    $DlowerDate = $DmaybeLessDate;
                                    ///$stockLevelrow['notification_days'] = $notificationDays;
                                }

                            }  else {
                                // $stockLevelrow['notification_days'] = '';
                                ///$stockLevelrow['expiry_date'] = $nested['expiry_date'];
                            }
                        }
                        // RemainingQty
                        //$remainingQty = $remainingQty + $nested['remaining_qty'] ;
                        //$stockLevelrow['remaining_qty'] = $remainingQty;
                        //file_put_contents("stockLevelrow$i.txt",  var_export($stockLevelrow, true));
                    }

                } catch (\Exception $e) {
                    file_put_contents( 'exception.txt',  $e->getMessage() . $e->getLine() );
                }


                if(!empty($lowerDate)) {
                    $lowerDate->add($Dinterval);
                    ///$stockLevelrow['expiry_date'] = $lowerDate->format('Y-m-d');
                }
                //var_dump($stockLevelrow['expiry_date']);
            

                $flat = array();
                $flat[] = $stockLevelrow;
                $rows = array_merge($flat, $nesteds);

            }// isset($aXML['StockLevel']['ExpiryDates'])

            // main colletion vector
            
            foreach ($rows as $row) {
                array_push($allRows, $row);
            }
        }
        //file_put_contents( 'stockrows.txt', var_export( $allRows , true));
        // return json encoded vector of row 
        return json_encode($allRows);
    }

}

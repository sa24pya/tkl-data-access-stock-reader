<?php
/**
 * File: tests/StockReaderTest.php
 *
 * @author mrx <silentstar@riseup.net>
 *
 * @package DataAccessStockReader
 * @subpackage StockReader
 * @version 1.0.3
 *
 */

namespace DataAccessStockReader;
use Dotenv\Dotenv;
/**
 * Class StockReaderTest
 *
 * @package DataAccessStockReader
 * @subpackage StockReader
 * @version 1.0.3
 */
class StockReaderTest extends \PHPUnit_Framework_TestCase
{

 	public function setUp() {}
    public function tearDown() {}
    

 	/**
     * Test person authenticate
     *
     * The line 204 in API must be commented in order to work
     */
    public function testStockReader()
    {
        $dotenv = new Dotenv(dirname( __DIR__));
        $dotenv->load();
        
        $result = StockReader::stockRead('5790000706808');
        $this->assertNotEmpty($result);

        /** @todo validate data-structure. **/

    }
}
